#pragma once
#include "Profiler.h"
class MemoryProfiler:public Profiler
{
public:
	MemoryProfiler();
	virtual ~MemoryProfiler();
	void StartProfiling(void) throw (std::exception);
	void StopProfiling(void)throw (std::exception);
	MemoryValues GetResults() const;
protected:
	void ResetValues(void);
private:
	void CalculateMemoryUsage() throw (std::exception);
	bool gIsMemoryProfilingStarted;
	MemoryValues gMemoryValues;
#ifdef WINDOWS
	void GetMemoryUsage(void) throw (std::exception);
#elif defined(LINUX)
	uint64_t parseLine(char* line);
	uint64_t getValueVirtualMemory();
	uint64_t getValueRam();
	void GetMemoryUsage(void) throw (std::exception);
#endif

#pragma region Virtual Memory
	uint64_t gTotalVirtualMemory;
	uint64_t gVirtualMemoryUsed;
	uint64_t gVirtualMemoryUsedByCurrentProcess;
#pragma endregion
#pragma region RAM Memory
	uint64_t gTotalRamMemory;
	uint64_t gRamMemoryUsed;
	uint64_t gRamMemoryUsedByCurrentProcess;
#pragma endregion
};


#include "stdafx.h"

#include "MemoryValues.h"


MemoryValues::MemoryValues()
{
}


MemoryValues::~MemoryValues()
{
}


double MemoryValues::GetVirtualMemTotalKB() const
{
	return TotalVirtualMemory / BYTE_TO_KB;
}
double MemoryValues::GetVirtualMemTotalMB() const
{
	return TotalVirtualMemory / BYTE_TO_MB;
}
double MemoryValues::GetVirtualMemTotalGB() const
{
	return TotalVirtualMemory / BYTE_TO_GB;
}

double MemoryValues::GetVirtualMemUsedKB() const
{
	return VirtualMemoryUsed / BYTE_TO_KB;
}
double MemoryValues::GetVirtualMemUsedMB() const
{
	return VirtualMemoryUsed / BYTE_TO_MB;
}
double MemoryValues::GetVirtualMemUsedGB() const
{
	return VirtualMemoryUsed / BYTE_TO_GB;
}


double MemoryValues::GetVirtualMemUsedCurrProcKB() const
{
	return VirtualMemoryUsedByCurrentProcess / BYTE_TO_KB;
}
double MemoryValues::GetVirtualMemUsedCurrProcMB() const
{
	return VirtualMemoryUsedByCurrentProcess / BYTE_TO_MB;
}
double MemoryValues::GetVirtualMemUsedCurrProcGB() const
{
	return VirtualMemoryUsedByCurrentProcess / BYTE_TO_GB;
}


double MemoryValues::GetRAMMemTotalKB() const
{
	return TotalRamMemory / BYTE_TO_KB;
}
double MemoryValues::GetRAMMemTotalMB() const
{
	return TotalRamMemory / BYTE_TO_MB;
}
double MemoryValues::GetRAMMemTotalGB() const
{
	return TotalRamMemory / BYTE_TO_GB;
}

double MemoryValues::GetRAMMemUsedKB() const
{
	return RamMemoryUsed / BYTE_TO_KB;
}
double MemoryValues::GetRAMMemUsedMB() const
{
	return RamMemoryUsed / BYTE_TO_MB;
}
double MemoryValues::GetRAMMemUsedGB() const
{
	return RamMemoryUsed / BYTE_TO_GB;
}


double MemoryValues::GetRAMMemUsedCurrProcKB() const
{
	return RamMemoryUsedByCurrentProcess / BYTE_TO_KB;
}
double MemoryValues::GetRAMMemUsedCurrProcMB() const
{
	return RamMemoryUsedByCurrentProcess / BYTE_TO_MB;
}
double MemoryValues::GetRAMMemUsedCurrProcGB() const
{
	return RamMemoryUsedByCurrentProcess / BYTE_TO_GB;
}

MemoryValues& MemoryValues::operator=(MemoryValues const &rhs) {
	if (this != &rhs) {
		delete m_ptr; // free resource;
		m_ptr = 0;
		m_ptr = rhs.m_ptr;
	}
	return *this;
};
#include "stdafx.h"
#include "Profiler.h"
#include "MemoryProfilingException.h"
#include "MemoryValues.h"
#include "MemoryProfiler.h"


MemoryProfiler::MemoryProfiler()
{
	gIsMemoryProfilingStarted = false;
}


MemoryProfiler::~MemoryProfiler()
{
}

void MemoryProfiler::StartProfiling(void) throw (std::exception)
{
	if (!gIsMemoryProfilingStarted)
	{
		ResetValues();
		gIsMemoryProfilingStarted = true;
		CalculateMemoryUsage();
	}
	else
	{
		throw MemoryProfilingException("Profiling already started exception!");
	}
}
void MemoryProfiler::StopProfiling(void)throw (std::exception)
{
	if (gIsMemoryProfilingStarted)
	{
		gIsMemoryProfilingStarted = false;
		//fill the fields
		gMemoryValues.TotalVirtualMemory = gTotalVirtualMemory;
		gMemoryValues.VirtualMemoryUsed = gVirtualMemoryUsed;
		gMemoryValues.VirtualMemoryUsedByCurrentProcess = gVirtualMemoryUsedByCurrentProcess;

		gMemoryValues.TotalRamMemory = gTotalRamMemory;
		gMemoryValues.RamMemoryUsed = gRamMemoryUsed;
		gMemoryValues.RamMemoryUsedByCurrentProcess = gRamMemoryUsedByCurrentProcess;




	}
	else
	{
		throw MemoryProfilingException("Profiling not started exception!");
	}
}

MemoryValues MemoryProfiler::GetResults() const
{
	return gMemoryValues;
}

void MemoryProfiler::CalculateMemoryUsage() throw (std::exception)
{
#ifdef WINDOWS
	GetMemoryUsage();
#elif defined(LINUX)

#endif
}

void MemoryProfiler::ResetValues(void)
{
	gTotalVirtualMemory = 0;
	gVirtualMemoryUsed = 0;
	gVirtualMemoryUsedByCurrentProcess = 0;
	gTotalRamMemory = 0;
	gRamMemoryUsed = 0;
	gRamMemoryUsedByCurrentProcess = 0;
}

#ifdef WINDOWS
void MemoryProfiler::GetMemoryUsage(void) throw (std::exception)
{
	MEMORYSTATUSEX memInfo;
	PROCESS_MEMORY_COUNTERS_EX pmc;
	ZeroMemory(&pmc, sizeof(PROCESS_MEMORY_COUNTERS_EX));
	memInfo.dwLength = sizeof(MEMORYSTATUSEX);
	if (GlobalMemoryStatusEx(&memInfo) != 0)
	{
		gTotalVirtualMemory = memInfo.ullTotalPageFile;
		gVirtualMemoryUsed = memInfo.ullTotalPageFile - memInfo.ullAvailPageFile;
		gTotalRamMemory = memInfo.ullTotalPhys;
		gRamMemoryUsed = memInfo.ullTotalPhys - memInfo.ullAvailPhys;
	}
	else
	{
		throw MemoryProfilingException("Memory Usage exception!");
	}
	if (GetProcessMemoryInfo(GetCurrentProcess(), (PROCESS_MEMORY_COUNTERS*)&pmc, sizeof(pmc)) != 0)
	{
		gVirtualMemoryUsedByCurrentProcess = pmc.PrivateUsage;
		gRamMemoryUsedByCurrentProcess = pmc.WorkingSetSize;
	}
	else
	{
		throw MemoryProfilingException("Cannot get the amount of memory used by current process!");
	}

}
#elif defined(LINUX)
void MemoryProfiler::GetMemoryUsage(void) throw (std::exception)
{
	struct sysinfo memInfo;
	if(sysinfo (&memInfo)==0)
	{
		gTotalVirtualMemory = (memInfo.totalram+memInfo.totalswap)*memInfo.mem_unit;
		gVirtualMemoryUsed = ((memInfo.totalram - memInfo.freeram)+(memInfo.totalswap - memInfo.freeswap))*memInfo.mem_unit;

		gTotalRamMemory = memInfo.totalram*memInfo.mem_unit;
		gRamMemoryUsed = (memInfo.totalram - memInfo.freeram)*memInfo.mem_unit;

		gVirtualMemoryUsedByCurrentProcess=getValueVirtualMemory();
		gRamMemoryUsedByCurrentProcess=getValueVirtualMemory();

	}
	else
	{
		throw MemoryProfilingException("Memory Usage exception!");
	}
	


}

uint64_t MemoryProfiler::parseLine(char* line){
    // This assumes that a digit will be found and the line ends in " Kb".
    size_t i = strlen(line);
    const char* p = line;
    while (*p <'0' || *p > '9') p++;
    line[i-3] = '\0';
    i = atoi(p);
    return i;
}

uint64_t MemoryProfiler::getValueRam(){ 
    FILE* file = fopen("/proc/self/status", "r");
    uint64_t result = 0;
    char line[128];

    while (fgets(line, 128, file) != NULL){
        if (strncmp(line, "VmRSS:", 6) == 0){
            result = parseLine(line);
            break;
        }
    }
    fclose(file);
    return (result*1024);
}

uint64_t MemoryProfiler::getValueVirtualMemory(){ //Note: this value is in KB!
    FILE* file = fopen("/proc/self/status", "r");
    uint64_t result = 0;
    char line[128];

    while (fgets(line, 128, file) != NULL){
        if (strncmp(line, "VmSize:", 7) == 0){
            result = parseLine(line);
            break;
        }
    }
    fclose(file);
    return (result*1024);
}


#endif
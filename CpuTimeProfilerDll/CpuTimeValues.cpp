#include "stdafx.h"
#include "CpuTimeValues.h"


CpuTimeValues::CpuTimeValues()
{

}

CpuTimeValues::~CpuTimeValues()
{

}

void CpuTimeValues::FillValues(const uint64_t cpu_time_ns, const uint64_t user_time_ns, const uint64_t kernel_time_ns)
{
	gKernelTimeNs = kernel_time_ns;
	gUserTimeNs = user_time_ns;
	gCpuTimeNs = cpu_time_ns;
}

uint64_t CpuTimeValues::GetCpuTimeNs() const
{
	return gCpuTimeNs;
}

uint64_t CpuTimeValues::GetUserTimeNs() const
{
	return gUserTimeNs;
}

uint64_t CpuTimeValues::GetKernelTimeNs() const
{
	return gKernelTimeNs;
}

double CpuTimeValues::GetCpuTimeUs() const
{
	return gCpuTimeNs / 1000.0;
}
double CpuTimeValues::GetUserTimeUs() const
{
	return gUserTimeNs / 1000.0;
}
double CpuTimeValues::GetKernelTimeUs() const
{
	return gKernelTimeNs / 1000.0;
}

double CpuTimeValues::GetCpuTimeMs() const
{
	return (gCpuTimeNs / 1.0E+6);
}
double CpuTimeValues::GetUserTimeMs() const
{
	return (gUserTimeNs / 1.0E+6);
}
double CpuTimeValues::GetKernelTimeMs() const
{
	return (gKernelTimeNs / 1.0E+6);
}
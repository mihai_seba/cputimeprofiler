#ifdef WINDOWS
#ifdef CPUTIMEPROFILERDLL_EXPORTS
#define CPUTIMEPROFILERDLL_API __declspec(dllexport)
#else
#define CPUTIMEPROFILERDLL_API __declspec(dllimport)
#endif
#elif defined(LINUX)
#define CPUTIMEPROFILERDLL_API 
#endif // _WIN32
#include "stdafx.h"
// This class is exported from the CpuTimeProfilerDll.dll
class CPUTIMEPROFILERDLL_API MemoryValues
{
public:
	MemoryValues();
	virtual ~MemoryValues();
	MemoryValues& operator=(MemoryValues const &rhs);
	
	double GetVirtualMemTotalKB() const;
	double GetVirtualMemTotalMB() const;
	double GetVirtualMemTotalGB() const;

	double GetVirtualMemUsedKB() const;
	double GetVirtualMemUsedMB() const;
	double GetVirtualMemUsedGB() const;


	double GetVirtualMemUsedCurrProcKB() const;
	double GetVirtualMemUsedCurrProcMB() const;
	double GetVirtualMemUsedCurrProcGB() const;


	double GetRAMMemTotalKB() const;
	double GetRAMMemTotalMB() const;
	double GetRAMMemTotalGB() const;

	double GetRAMMemUsedKB() const;
	double GetRAMMemUsedMB() const;
	double GetRAMMemUsedGB() const;


	double GetRAMMemUsedCurrProcKB() const;
	double GetRAMMemUsedCurrProcMB() const;
	double GetRAMMemUsedCurrProcGB() const;




	uint64_t TotalVirtualMemory;
	uint64_t VirtualMemoryUsed;
	uint64_t VirtualMemoryUsedByCurrentProcess;
	uint64_t TotalRamMemory;
	uint64_t RamMemoryUsed;
	uint64_t RamMemoryUsedByCurrentProcess;
private:
	const double BYTE_TO_KB = 1024.0;
	const double BYTE_TO_MB = std::pow(2, 20);
	const double BYTE_TO_GB = std::pow(2, 30);
	MemoryValues *m_ptr;
};


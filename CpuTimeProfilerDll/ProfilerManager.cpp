#include "stdafx.h"
#include "Profiler.h"
#include "CpuTimeProfilingException.h"
#include "MemoryProfilingException.h"
#include "CpuTimeValues.h"
#include "MemoryValues.h"
#include "CpuTimeProfiler.h"
#include "MemoryProfiler.h"

#include "ProfilerManager.h"


ProfilerManager::ProfilerManager()
{
	gCpuTimeProfiler = std::make_unique<CpuTimeProfiler>();
	gMemoryProfiler = std::make_unique<MemoryProfiler>();
	gErrorString = "";
	gIsCpuProfilerStarted = false;
	gIsMemProfilerStarted = false;
}


ProfilerManager::~ProfilerManager()
{
	gCpuTimeProfiler.reset(nullptr);
	gMemoryProfiler.reset(nullptr);
}


void ProfilerManager::RunProfiler(E_ProfilingOption option)
{
	if (option == OptionCpuTime)
	{
		try {
			gIsCpuProfilerStarted = true;
			gCpuTimeProfiler->StartProfiling();
		}
		catch (const CpuTimeProfilingException& e) {
			gErrorString = e.what();
		}
	}
	else
	{
		if (option == OptionMemoryUsage)
		{
			try {
				gIsMemProfilerStarted = true;
				gMemoryProfiler->StartProfiling();
			}
			catch (const CpuTimeProfilingException& e) {
				gErrorString = e.what();
			}
		}
	}

}

void ProfilerManager::PreprocessResultsAndStop()
{
	if (gIsCpuProfilerStarted)
	{
		try {
			gIsCpuProfilerStarted = false;
			gCpuTimeProfiler->StopProfiling();
		}
		catch (const CpuTimeProfilingException& e) {
			gErrorString = e.what();
		}
	}
	else
	{
		if (gIsMemProfilerStarted)
		{
			try {
				gIsMemProfilerStarted = false;
				gMemoryProfiler->StopProfiling();
			}
			catch (const MemoryProfilingException& e) {
				gErrorString = e.what();
			}
		}
	}
}

bool ProfilerManager::IsError() const
{
	return !(gErrorString.empty());
}

std::string ProfilerManager::GetErrorMessage() const
{
	return gErrorString;
}

CpuTimeValues ProfilerManager::GetCpuTimeProfilerResults() const
{
	CpuTimeValues cpuTimeValuesResults;
	auto d = dynamic_cast<CpuTimeProfiler*>(gCpuTimeProfiler.get());
	
	d->FillProfilerValues(&cpuTimeValuesResults);

	
	return cpuTimeValuesResults;
}

MemoryValues ProfilerManager::GetMemoryUsage() const
{
	
	auto d = dynamic_cast<MemoryProfiler*>(gMemoryProfiler.get());
	 
	return d->GetResults();

}
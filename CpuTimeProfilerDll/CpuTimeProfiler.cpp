// CpuTimeProfilerDll.cpp : Defines the exported functions for the DLL application.
//

#include "stdafx.h"
#include "CpuTimeProfilingException.h"
#include "CpuTimeValues.h"
#include "Profiler.h"
#include "CpuTimeProfiler.h"

CpuTimeProfiler::CpuTimeProfiler()
{
	gIsCpuProfilingStarted = false;
}

CpuTimeProfiler::~CpuTimeProfiler()
{

}

void CpuTimeProfiler::ResetValues()
{
	gCpuTime = 0;
	gUserTime = 0;
	gKernelTime = 0;
}
void CpuTimeProfiler::StartProfiling(void) throw (std::exception)
{
	if (!gIsCpuProfilingStarted)
	{
		ResetValues();
		gIsCpuProfilingStarted = true;
		CalculateCpuTime();
			

	}
	else
	{
		throw CpuTimeProfilingException("Profiling already started exception!");
	}
}
void CpuTimeProfiler::StopProfiling(void) throw (std::exception)
{
	
	if (gIsCpuProfilingStarted)
	{
		gIsCpuProfilingStarted = false;
		CalculateCpuTime();
	
	}
	else
	{
		throw CpuTimeProfilingException("Profiling not started exception!");
	}
}

void CpuTimeProfiler::FillProfilerValues(CpuTimeValues *cpuTimeValues)
{
	(*cpuTimeValues).FillValues(gCpuTime, gUserTime, gKernelTime);
}

void CpuTimeProfiler::CalculateCpuTime() throw (std::exception)
{
	uint64_t initialUserTime;
	uint64_t initialKernelTime;
	uint64_t initialCpuTime;

	/*initialization*/
	initialUserTime = gUserTime;
	initialKernelTime = gKernelTime;
	initialCpuTime = gCpuTime;
#ifdef WINDOWS
	GetWindowsCpuTime(initialUserTime, initialKernelTime, initialCpuTime);
#elif defined(LINUX)
	GetLinuxCpuTime(initialUserTime, initialKernelTime, initialCpuTime);
#endif

}

#ifdef WINDOWS
void CpuTimeProfiler::GetWindowsCpuTime(const uint64_t initUserTime, const uint64_t initKernelTime, const uint64_t initCpuTime) throw (CpuTimeProfilingException)
{
	
	FILETIME pCreationTime, pExitTime, pKernelTime, pUserTime;
	if (GetProcessTimes(GetCurrentProcess(), &pCreationTime, &pExitTime, &pKernelTime, &pUserTime) != 0){

		gUserTime = (pUserTime.dwLowDateTime | ((uint64_t)pUserTime.dwHighDateTime << 32)) ;
		gKernelTime = (pKernelTime.dwLowDateTime | ((uint64_t)pKernelTime.dwHighDateTime << 32)) ;
		gCpuTime = gUserTime + gKernelTime;
	
		gUserTime -= initUserTime;
		gKernelTime -= initKernelTime;
		gCpuTime -= initCpuTime;
	
	}
	else{
		throw CpuTimeProfilingException("Process time error exception!");
	}

}
#elif defined(LINUX)
void CpuTimeProfiler::GetLinuxCpuTime(const uint64_t initUserTime, const uint64_t initKernelTime, const uint64_t initCpuTime) throw (std::exception)
{
	struct rusage usage;
	struct timeval valueKernelTime;
	struct timeval valueUserTime;
	if(getrusage(RUSAGE_SELF, &usage)==0)
	{
		valueUserTime=usage.ru_utime;
		valueKernelTime=usage.ru_stime;
		gUserTime=(valueUserTime.tv_sec*1E+9)+(valueUserTime.tv_usec*1000);
		gKernelTime=(valueKernelTime.tv_sec*1E+9)+(valueKernelTime.tv_usec*1000);
		gCpuTime=gUserTime+gKernelTime;

		gUserTime -= initUserTime;
		gKernelTime -= initKernelTime;
		gCpuTime -= initCpuTime;

		
	}
	else
	{
		throw CpuTimeProfilingException(std::string(strerror(errno)));
	}

}
#endif


